<?php

/**
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class System {

    static function getFrontendTemplate($template, $folder = '') {
        return MAIN_SERVER_FOLDER . '/view/templates/themes/standard/frontend/' . (!empty($folder) ? $folder . '/' : '') . $template;
    }

    static function getTemplateUrl() {
        return WEB_URL . '/wiew/templates/themes/standard';
    }

    static function getTemplate() {
        return blue_ghost_WebTemplate::getInstance();
    }

    static function redirect($url) {
        header("Location: " . $url);
        die();
    }

}
