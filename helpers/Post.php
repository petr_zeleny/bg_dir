<?php

/**
 * Description of Post
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class Post {
    static function get($paramName, $default = null) {
        return (isset($_POST[$paramName]) ? $_POST[$paramName] : $default);
    }
}

?>
