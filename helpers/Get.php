<?php
/**
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class Get {

    var $parameters;

    function __construct($parameters = array()) {
        $this->parameters = $parameters;
    }

    public function setParam($paramName, $value) {
        $this->parameters[$paramName] = $value;
    }

    public function getParam($paramName, $default = null) {
        return (isset($this->parameters[$paramName]) ? $this->parameters[$paramName] : $default);
    }

    static function get($paramName, $default = null) {
        return (isset($_GET[$paramName]) ? $_GET[$paramName] : $default);
    }

    public function getRef($basicAddress = WEB_ADDRESS) {
        if (sizeof($this->parameters) > 0) {
            $basicAddress.='?';
            foreach ($this->parameters as $key => $value) {
                $basicAddress.=$key . '=' . $value . '&amp;';
            }
            return substr($basicAddress, 0, -5); //odstrani posledni &amp;
        } else {
            return $basicAddress;
        }
    }

}

?>
