<?php

/**
 * Description of Helper
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class Helper {


    static function filter_output($output, $callback = '') {
        return static::xss_clean($output);
    }

    static function xss_clean($data) {
        // Fix &entity\n;
        $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while ($old_data !== $data);

        // we are done...
        return $data;
    }



    static function unaccent($name) {
        $new_url = $name;
        $prevodni_tabulka = Array(
            'ä' => 'a',
            'Ä' => 'a',
            'á' => 'a',
            'Á' => 'a',
            'à' => 'a',
            'À' => 'a',
            'ã' => 'a',
            'Ã' => 'a',
            'â' => 'a',
            'Â' => 'a',
            'č' => 'c',
            'Č' => 'c',
            'ć' => 'c',
            'Ć' => 'c',
            'ď' => 'd',
            'Ď' => 'd',
            'ě' => 'e',
            'Ě' => 'e',
            'é' => 'e',
            'É' => 'e',
            'ë' => 'e',
            'Ë' => 'e',
            'è' => 'e',
            'È' => 'e',
            'ê' => 'e',
            'Ê' => 'e',
            'í' => 'i',
            'Í' => 'i',
            'ï' => 'i',
            'Ï' => 'i',
            'ì' => 'i',
            'Ì' => 'i',
            'î' => 'i',
            'Î' => 'i',
            'ľ' => 'l',
            'Ľ' => 'l',
            'ĺ' => 'l',
            'Ĺ' => 'l',
            'ń' => 'n',
            'Ń' => 'n',
            'ň' => 'n',
            'Ň' => 'n',
            'ñ' => 'n',
            'Ñ' => 'n',
            'ó' => 'o',
            'Ó' => 'o',
            'ö' => 'o',
            'Ö' => 'o',
            'ô' => 'o',
            'Ô' => 'o',
            'ò' => 'o',
            'Ò' => 'o',
            'õ' => 'o',
            'Õ' => 'o',
            'ő' => 'o',
            'Ő' => 'o',
            'ř' => 'r',
            'Ř' => 'r',
            'ŕ' => 'r',
            'Ŕ' => 'r',
            'š' => 's',
            'Š' => 's',
            'ś' => 's',
            'Ś' => 's',
            'ť' => 't',
            'Ť' => 't',
            'ú' => 'u',
            'Ú' => 'u',
            'ů' => 'u',
            'Ů' => 'u',
            'ü' => 'u',
            'Ü' => 'u',
            'ù' => 'u',
            'Ù' => 'u',
            'ũ' => 'u',
            'Ũ' => 'u',
            'û' => 'u',
            'Û' => 'u',
            'ý' => 'y',
            'Ý' => 'y',
            'ž' => 'z',
            'Ž' => 'z',
            'ź' => 'z',
            'Ź' => 'z',
            '&' => 'a',
            '?' => '-',
            '.' => '',
            ',' => '',
            '!' => '',
            '/' => '',
            '(' => '',
            ')' => '',
            '}' => '',
            '{' => '',
            ':' => '',
            '<' => '',
            '>' => '',
            '%' => '',
            '#' => '',
            '*' => '',
            '@' => '',
            '+' => '',
            '"' => '',
            "'" => '',
            "~" => '',
            "°" => '',
        );
        foreach ($prevodni_tabulka as $letter => $let) {
            $new_url = str_replace($letter, $let, $new_url);
        }
        $new_url = str_replace(' ', '-', $new_url);
        return strtolower($new_url);
    }

}
