<?php

/**
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class Session {

    private $data = array();

    public function __construct() {

        if (!session_id()) {
            ini_set('session.use_cookies', 'On');
            ini_set('session.use_trans_sid', 'Off');
            ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'], strpos($_SERVER['SERVER_NAME'], "."), 100));
            session_set_cookie_params(SESSION_LIVE_TIME, '/');
            session_start();
        }

        $this->data = & $_SESSION;
    }

    function getId() {
        return session_id();
    }

    public function get($key, $default = null) {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return $default;
    }

    public function set($key, $value) {
        $this->data[$key] = $value;
    }

    public function delete($key) {
        if (isset($this->data[$key])) {
            unset($this->data[$key]);
        }
    }

    public function destroy() {
        session_destroy();
    }

}

?>
