<?php

/*
 * All files to include
 */

include_once __DIR__ . "/lang.php";
include_once MAIN_SERVER_FOLDER . '/view/templates/BaseTemplate.php';
include_once MAIN_SERVER_FOLDER . '/view/templates/themes/blue_ghost/blue_ghost_WebTemplate.php';
include_once MAIN_SERVER_FOLDER . '/helpers/Session.php';
include_once MAIN_SERVER_FOLDER . '/helpers/System.php';
include_once MAIN_SERVER_FOLDER . '/helpers/Post.php';
include_once MAIN_SERVER_FOLDER . '/helpers/Get.php';
include_once MAIN_SERVER_FOLDER . '/helpers/Helper.php';
include_once MAIN_SERVER_FOLDER . '/controller/Router.php';
include_once MAIN_SERVER_FOLDER . '/controller/BaseController.php';
include_once MAIN_SERVER_FOLDER . '/controller/PersonItemController.php';
include_once MAIN_SERVER_FOLDER . '/model/BaseModel.php';
include_once MAIN_SERVER_FOLDER . '/model/XmlModel.php';
include_once MAIN_SERVER_FOLDER . '/model/PersonItemModel.php';

include_once MAIN_SERVER_FOLDER . '/view/templates/CustomSmarty.php';
date_default_timezone_set('Europe/Prague');
?>
