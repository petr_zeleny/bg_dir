<?php

/**
 * Description of XmlModel
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class XmlModel extends BaseModel {

    //put your code here
    protected $xmlSource;
    protected $dom;
    protected $itemsCount;

    function __construct($xmlName = 'xml.xml') {
        $this->xmlSource = XML_DIR . $xmlName;
        if (!file_exists($this->xmlSource)) {
            $this->createFileSource($this->xmlSource);
        } else {
            $this->dom = new DOMDocument();
            $this->dom->formatOutput = true;
            $this->dom->load($this->xmlSource);
        }
    }

    private function createFileSource($filePath) {
        $this->dom = new DOMDocument('1.0', 'UTF-8');
        $this->dom->formatOutput = true;
        $root = $this->dom->createElement('items');
        $this->dom->appendChild($root);
        $this->dom->save($filePath);
    }

    public function getItems($itemName, $offset = 0, $limit = DEFAULT_ITEM_COUNT) {
        $nodes = $this->dom->getElementsByTagname($itemName);
        $items = array();
        $this->itemsCount = $nodes->length;
        if ($limit == -1 && $offset == 0) {
            foreach ($nodes as $n) {
                $items[] = $this->formatItem($n);
            }
        } else {
            foreach ($nodes as $n) {
                if ($offset-- > 0) {
                    continue;
                }

                if ($limit == -1) {
                    $items[] = $this->formatItem($n);
                } else {

                    if ($limit-- > 0) {
                        $items[] = $this->formatItem($n);
                    } else {
                        return $items;
                    }
                }
            }
        }


        return $items;
    }

    protected function formatItem($node) {
        return $node;
    }

    public function getItem($id) {
        return null;
    }

    public function deleteItem($id) {
        return null;
    }

    public function itemExist($id) {
        return false;
    }

    public function getItemsCountWithoutLimit() {
        return $this->itemsCount;
    }

}
