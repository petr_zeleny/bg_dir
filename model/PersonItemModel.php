<?php

/**
 * Description of PersonItemModel
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class PersonItemModel extends XmlModel {

    function __construct($xmlName = XML_NAME) {
        parent::__construct($xmlName);
    }

    protected function formatItem($node) {
        if (empty($node)) {
            return array();
        }
        $id = $node->getAttribute('id');
        $item = array();
        $item['id'] = $id;
        $item['name'] = Helper::xss_clean($node->getElementsByTagName('name')->item(0)->nodeValue);
        $item['phone'] = Helper::xss_clean($node->getElementsByTagName('phone')->item(0)->nodeValue);
        $item['email'] = Helper::xss_clean($node->getElementsByTagName('email')->item(0)->nodeValue);
        $item['note'] = Helper::xss_clean($node->getElementsByTagName('note')->item(0)->nodeValue);
        return $item;
    }

    public function saveItem($data, $id = null) {
        $this->dom->formatOutput = true;
        $root = $this->dom->getElementsByTagName('items')->item(0);

        $itemElement = $this->dom->createElement('person', '');
        $elementId = $this->dom->createAttribute('id');
        $elementId->value = $id;

        $nameElement = $this->dom->createElement('name');
        $nameElement->appendChild($this->dom->createCDATASection($data['name']));
        $emailElement = $this->dom->createElement('email');
        $emailElement->appendChild($this->dom->createCDATASection($data['email']));
        $phoneElement = $this->dom->createElement('phone');
        $phoneElement->appendChild($this->dom->createCDATASection($data['phone']));
        $noteElement = $this->dom->createElement('note');
        $noteElement->appendChild($this->dom->createCDATASection($data['note']));

        $itemElement->appendChild($elementId);
        $itemElement->appendChild($nameElement);
        $itemElement->appendChild($emailElement);
        $itemElement->appendChild($phoneElement);
        $itemElement->appendChild($noteElement);
        $root->appendChild($itemElement);

        $this->dom->save($this->xmlSource);
        return true;
    }

    public function deleteItem($id) {
        $xpath = new DOMXpath($this->dom);
        $elements = $xpath->query("//person[@id='$id']");
        if (!empty($elements)) {
            $item = $elements->item(0);
            if (empty($item)) {
                return false;
            }
            $item->parentNode->removeChild($item);
            $this->dom->formatOutput = true;
            $this->dom->save($this->xmlSource);
            return true;
        }
        return false;
    }

    public function updateItem($data, $id) {
        $xpath = new DOMXpath($this->dom);
        $elements = $xpath->query("//person[@id='$id']");
        if (!empty($elements)) {
            $item = $elements->item(0);
            $item->parentNode->removeChild($item);
            return $this->saveItem($data, $data['id']);
        }
    }

    public function getItem($id) {
        $xpath = new DOMXpath($this->dom);

        $elements = $xpath->query("//person[@id='$id']");
        if (!empty($elements)) {
            $item = $elements->item(0);
            return $this->formatItem($item);
        }
        return null;
    }

    public function itemExist($id) {
        $xpath = new DOMXpath($this->dom);
        $elements = $xpath->query("//person[@id='$id']");
        return (intval($elements->length) > 0);
    }

}
