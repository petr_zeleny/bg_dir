<?php

/**
 * Description of BaseModel
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class BaseModel {

    function __construct() {
        
    }

    function getData() {
        return $this->data;
    }

    function get($item) {
        return $this->data[$item];
    }

    function set($item, $value) {
        return $this->data[$item] = $value;
    }

    public function getItem($id) {
        
    }

    public function saveItem($data, $id=null) {
        
    }

    public function updateItem($data, $id) {
        
    }

    public function deleteItem($id) {
        
    }

}
