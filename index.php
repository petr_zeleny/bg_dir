<?php 
include_once 'lang/includes.php';
set_error_handler('frontend_error_handler');
$session = new Session();
$url = (isset($_GET['url'])?$_GET['url'] : '');
$router = new Router($url);
$router->route();



function frontend_error_handler($errno, $errstr, $errfile, $errline) {
    switch ($errno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if (DEBUG) {
        echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
    }

    return true;
}
?>