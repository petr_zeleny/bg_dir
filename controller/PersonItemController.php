<?php

/**
 * Description of IndexController
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class PersonItemController extends BaseController {

    public function index() {
        $action = Get::get('action');
        switch ($action) {
            case 'addItem':
                $this->addItem();
                break;
            case 'editItem':
                $this->editItem(Get::get('id'));
                break;
            case 'deleteItem':
                $this->deleteItem(Get::get('id'));
                break;
            default: $this->itemList();
        }
    }

    public function render() {
        $template = System::getTemplate();
        $template->setMetaData('', '', $this->metaData);
        $template->setHtml($this->html);
        return $template->renderSite();
    }

    public function itemList() {
        $this->metaData['title'] = 'Adresář';
        $model = new PersonItemModel();
        $actualPage = Get::get('page', 1);
        $items = $model->getItems('person', ($actualPage - 1) * DEFAULT_ITEM_COUNT);
        $itemsCount = $model->getItemsCountWithoutLimit();
        $template_site = new CustomSmarty();
        $template_site->assign('items', $items);
        $template_site->assign('paginator', $this->createPaginator($itemsCount));
        $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('list.tpl', 'person'));
    }

    public function editItem($id) {
        $this->metaData['title'] = 'Úprava položky ' . $id;
        $model = new PersonItemModel();
        $template_site = new CustomSmarty();
        if (empty($_POST)) {
            $session = new Session();
            $saved = $session->get('person-' . $id);
            if ($saved) {
                $template_site->assign('state', true);
                $session->delete('person-' . $id);
            }
            $model = new PersonItemModel();
            $data = $model->getItem($id);
            if (empty($data)) {
                return $this->itemList();
            }
            $template_site->assign('data', $data);
            $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('form.tpl', 'person'));
        } else {
            $data = Post::get('person');
            $err = $this->validateItem($data);
            if (empty($err)) {
                $id = $this->createIdFromName($data['name']);
                $id = $this->createUniqueId($id);
                $old_id = $data['id'];
                $data['id'] = $id;
                $saved = $model->updateItem($data, $old_id);
                $template_site->assign('data', $data);
                if ($saved) {
                    $session = new Session();
                    $session->set('person-' . $id, true);
                    System::redirect(WEB_URL . '/' . $id . '');
                } else {
                    $template_site->assign('state', false);
                }
            } else {
                $template_site->assign('errors', $err);
                $template_site->assign('data', $data);
            }

            $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('form.tpl', 'person'));
        }
    }

    public function addItem() {
        $this->metaData['title'] = 'Nová položka';
        $model = new PersonItemModel();
        $template_site = new CustomSmarty();
        if (empty($_POST)) {
            $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('form.tpl', 'person'));
        } else {
            $data = Post::get('person');
            $err = $this->validateItem($data);
            $template_site->assign('data', $data);
            if (empty($err)) {
                $id = $this->createIdFromName($data['name']);
                $id = $this->createUniqueId($id);
                $saved = $model->saveItem($data, $id);
                if ($saved) {
                    $session = new Session();
                    $session->set('person-' . $id, true);
                    System::redirect(WEB_URL . '/' . $id . '');
                } else {
                    $template_site->assign('state', false);
                }
            } else {
                $template_site->assign('errors', $err);
            }

            $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('form.tpl', 'person'));
        }
    }

    public function deleteItem($id) {
        $this->metaData['title'] = 'Smazání položky ' . $id;
        $template_site = new CustomSmarty();
        $template_site->assign('id', $id);
        if (Post::get('delete_person', null) != null) {
            $model = new PersonItemModel();
            $deleted = $model->deleteItem($id);
            $template_site->assign('state', $deleted);
        }
        $this->html = $template_site->fetch(BaseTemplate::localizeFrontendTemplate('confirm_delete.tpl', 'person'));
    }

    private function createUniqueId($id) {
        $model = new PersonItemModel();
        $base_id = $id;
        $index = 1;
        while (true) {

            if ($model->itemExist($id)) {
                $id = $base_id . '-' . $index++;
            } else {
                return $id;
            }
        }
    }

    private function createIdFromName($name) {
        $id = Helper::unaccent($name);
        return $id;
    }

    protected function createPaginator($itemCount, $itemsOnPage = DEFAULT_ITEM_COUNT) {
        if (empty($itemCount) && $itemCount <= $itemsOnPage) {
            return array();
        }
        $paginator = array();
        $pages = ($itemCount / $itemsOnPage) + ($itemCount % $itemsOnPage > 0 ? 1 : 0);
        $activePage = Get::get('page', 1);
        for ($i = 1; $i <= $pages; $i++) {
            $paginator[] = array(
                'number' => $i,
                'url' => '?page=' . $i,
                'active' => ($activePage == $i));
        }

        return $paginator;
    }

    private function validateItem($data) {
        $err = array();
        if (empty($data['name'])) {
            $err[] = 'Vyplňte prosím jméno';
        }
        if (empty($data['phone'])) {
            $err[] = 'Vyplňte prosím telefon';
        }
        if (empty($data['email'])) {
            $err[] = 'Vyplňte prosím email';
        } else {
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $err[] = "Emailová adresa není ve správném tvaru";
            }
        }

        return $err;
    }

}
