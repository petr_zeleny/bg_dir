<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class Router {

    //put your code here
    var $url;
    var $url_parts;

    function __construct($url) {
        $url = trim($url);
        $url = rtrim($url, '/');
        $this->url = $url;
        if (!empty($this->url)) {
            $this->url_parts = explode('/', $this->url);
        } else {
            $this->url_parts = array();
        }
    }

    public function route() {
        $controller = null;
        $action = Get::get('action');
        try {



            if (isset($this->url_parts[0]) && empty($action)) {
                $controller = new PersonItemController();
                $controller->editItem($this->url_parts[0]);
            } else if (isset($this->url_parts[0])) {
                switch ($action) {
                    case 'deleteItem':
                        $controller = new PersonItemController();
                        $controller->deleteItem($this->url_parts[0]);

                        break;
                }
            } else {

                $controller = new PersonItemController();
                $controller->index();
            }

            $html = $controller->render();
            echo $html;
        } catch (Exception $exc) {
            echo 'Ups....';
        }
    }

}
