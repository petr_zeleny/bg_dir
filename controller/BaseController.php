<?php

/**
 * Description of BaseController
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class BaseController {

    var $html;
    var $metaData;

    function __construct() {
        
    }

    public function getEditorForm() {
        
    }

    public function getObject() {
        
    }

    public function saveObject() {
        
    }

    public function deleteObject() {
        
    }

    public function render() {
        return '';
    }

}
