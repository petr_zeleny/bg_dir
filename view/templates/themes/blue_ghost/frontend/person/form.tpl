
<div class="form">
    {if isset($data.id)}
        <h2>Editace položky</h2>
    {else}
        <h2>Nová položka</h2>
    {/if}

    <form method="post" action="">
        {if isset($state) && $state==true}
            <strong style="color:green;">Záznam byl uložen</strong><br/>

        {/if}
        {if isset($state) && $state==false}
            <strong style="color:red;">Záznam nebyl uložen</strong><br/>

        {/if}
        {if isset($errors)}
            {foreach from=$errors key=key item=error}
                <strong style="color:red"> {$error}</strong><br/>
            {/foreach}
        {/if}

        <label for="name">Jméno</label><br/>
        <input type="text" id="name" name="person[name]" value="{if isset($data.name)}{$data.name}{/if}">
        <br/>
        <label for="name">Email</label><br/>
        <input type="text" id="email"  name="person[email]" value="{if isset($data.email)}{$data.email}{/if}">
        <br/>
        <label for="name">Telefon</label><br/>
        <input type="text" id="phone"  name="person[phone]" value="{if isset($data.phone)}{$data.phone}{/if}">
        <br/>
        <label for="name">Poznámka</label><br/>
        <textarea id="note"  name="person[note]">{if isset($data.note)}{$data.note}{/if}</textarea>
        <br/>
        {if isset($data.id)}
            <input type="hidden" name="person[id]" value="{$data.id}">
        {/if}
        <input type="submit" name="save_person" value="{if isset($data.id)}Upravit{else}Uložit{/if}">
    </form>

    <hr/>
    <a href="{$smarty.const.WEB_URL}">Zpět na seznam</a>

</div>
