
<div class="list">

    {if !empty($items)}
        <table  style="width: 100%;" border="1" class="table table-striped">
            <thead>
                <tr>
                    <th>Jméno</th>
                    <th>Telefon</th>
                    <th>Email</th>
                    <th>Poznámka</th>
                    <th>Akce</th>
                </tr>
            </thead>
            {foreach from=$items key=key item=item}
                <tr>
                    <td>{$item.name}</td>
                    <td>{$item.phone}</td>
                    <td>{$item.email}</td>
                    <td>{$item.note}</td>
                    <td>
                        <a href="{$smarty.const.WEB_URL}/{$item.id}">Upravit</a>
                        <a href="{$smarty.const.WEB_URL}/{$item.id}?action=deleteItem">Smazat</a>
                    </td>
                </tr>
            {/foreach}

        </table>
        <hr/>
        {if isset($paginator)}
            <h3>Další záznamy</h3>
            <ul>
                {foreach from=$paginator key=key item=item}
                    <li><a href="{$item.url}">{if $item.active}<strong>{$item.number}</strong>{else}{$item.number}{/if}</a></li>
                            {/foreach} 
            </ul>
        {/if}

    {else}
        <strong>Žádné záznamy</strong>
    {/if}

    <hr/>
    <a href="?action=addItem">Přidat položku</a>



</div>
