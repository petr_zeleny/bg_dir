
<div class="form">
    <h2>Smazání položky - {$id}</h2>

    {if isset($state) && $state==true}
        <strong style="color:green;">Záznam byl smazám</strong><br/>

    {/if}
    {if isset($state) && $state==false}
        <strong style="color:red;">Záznam nebyl smazán</strong><br/>

    {/if}

    {if !isset($state)}
    <form method="post" action="">
      
    {if isset($errors)}
        {foreach from=$errors key=key item=error}
                <strong style="color:red"> {$error}</strong><br/>
        {/foreach}
    {/if}


    {if isset($id)}
            <input type="hidden" name="person[id]" value="{$data.id}">
    {/if}
        <input type="submit" name="delete_person" value="Potvrdit smazání">

    </form>
        
    {/if}

    <hr/>
    <a href="{$smarty.const.WEB_URL}">Zpět na seznam</a>

</div>
