<!DOCTYPE html>
<html class="no-js"  lang="{if isset($meta_data.language) && !empty($meta_data.language)}{$meta_data.language}{else}cs{/if}">
    <head>
        <title>{if isset($meta_data.title) && !empty($meta_data.title)}{$meta_data.title}{else}BlueGhost{/if}</title>
        <meta http-equiv="Content-Type" content="text/html; charset={if isset($meta_data.encoding) && !empty($meta_data.encoding)}{$meta_data.encoding}{else}UTF-8{/if}" />
        <meta name="description" content="{if isset($meta_data.description) && !empty($meta_data.description)}{$meta_data.meta_description}{/if}"/>
        <meta name="keywords" content="{if isset($meta_data.meta_key) && !empty($meta_data.meta_key)}{$meta_data.meta_key}{/if}" />

        {if isset($meta_data.index_robots) && $meta_data.index_robots==true}
            <meta name="robots" content="index, follow"/>

        {else}
            <meta name="robots" content="noindex, nofollow"/>
        {/if}

        <meta name="Author" content="Petr Zeleny" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        {if isset($meta_data.favicon)}
            <link rel="Shortcut Icon" href="{$meta_data.favicon}" type="image/x-icon" />
        {/if}
        {foreach from=$meta_data.css key=klic item=uri}
            <link rel="stylesheet" type="text/css" href="{$uri}" media="screen, print" />

        {/foreach}


        {foreach from=$meta_data.js key=klic item=uri}
            <script type="text/javascript" src="{$uri}" ></script>
        {/foreach}

    </head>