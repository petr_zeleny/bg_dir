<?php

/**
 */
class blue_ghost_WebTemplate extends BaseTemplate {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function setMetaData($title = '', $description = '', $attr = array()) {
        $attr['index_robots'] = false;
        $attr['css'] = array();
        $attr['js'] = array();
        parent::setMetaData($title, $description, $attr);
    }

}

?>
