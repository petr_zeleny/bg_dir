<?php

/**
 * Description of CustomSmarty
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
require 'libs/Smarty.class.php';

class CustomSmarty extends Smarty {

    public function __construct() {
        parent::__construct();
        $this->template_dir = MAIN_SERVER_FOLDER . '/view/templates/themes';
        $this->config_dir = MAIN_SERVER_FOLDER . '/view/templates/configs/';
        $this->compile_dir = MAIN_SERVER_FOLDER . '/view/templates/templates_c/';
        $this->cache_dir = MAIN_SERVER_FOLDER . '/view/templates/cache/';
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

}
?>
