<?php

/**
 * Description of BaseTemplate
 *
 * @author Petr Zeleny <zeleny@webgal.cz>
 */
class BaseTemplate {

    var $metaData;
    var $html;

    static function localizeFrontendTemplate($template, $folder = '') {
        if (is_file(TEMPLATE_DIR . '/frontend/' . (!empty($folder) ? $folder . '/' : '') . $template)) {
            return TEMPLATE_DIR . '/frontend/' . (!empty($folder) ? $folder . '/' : '') . $template;
        } else {
            return TEMPLATE_DIR . '/frontend/sites/404.tpl';
        }
    }

    static protected function localizeCss($css, $folder = '') {
        return WEB_URL . '/view/templates/themes/standard/css/' . (!empty($folder) ? $folder . '/' : '') . $css;
    }

    static protected function localizeJs($js, $folder = '') {
        return WEB_URL . '/view/templates/themes/standard/js/' . (!empty($folder) ? $folder . '/' : '') . $js;
    }

    static protected function localizeEditorTemplate($template, $folder = '') {
        return MAIN_SERVER_FOLDER . '/view/templates/themes/editor/frontend/' . (!empty($folder) ? $folder . '/' : '') . $template;
    }

    public function setMetaData($title = '', $description = '', $attr = array()) {
        if (!is_array($this->metaData)) {
            $this->metaData = array();
        }
        $this->metaData['title'] = $title;
        $this->metaData['description'] = $description;
        if (!empty($attr)) {
            $this->metaData = array_merge($this->metaData, $attr);
        }
    }

    protected function getPageHeader() {
        $template_site = new CustomSmarty();
        $template_site->assign('meta_data', $this->metaData);
        return $template_site->fetch(BaseTemplate::localizeFrontendTemplate('system_header.tpl', 'common'));
    }

    protected function getPageFooter() {
        
    }

    public function setHtml($html) {
        $this->html = $html;
    }

    public function renderSite($params = array()) {
        $template_site = new CustomSmarty();
        $template_site->assign('system_header', $this->getPageHeader());
        $template_site->assign('system_footer', $this->getPageFooter());
        $template_site->assign('html', $this->html);
        return $template_site->fetch(BaseTemplate::localizeFrontendTemplate('main_site_template.tpl'));
    }

}
