<?php /* Smarty version Smarty-3.1.15, created on 2019-02-22 10:00:11
         compiled from "C:\xampp\htdocs\bg_dir\view\templates\themes\blue_ghost\\frontend\common\system_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182045c6fba1b16f667-50024404%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9a5d2e6ca5bcbe08efef276509bad675a96ad7e2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\bg_dir\\view\\templates\\themes\\blue_ghost\\\\frontend\\common\\system_header.tpl',
      1 => 1550781075,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182045c6fba1b16f667-50024404',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'meta_data' => 0,
    'uri' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5c6fba1b1f0509_16802000',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c6fba1b1f0509_16802000')) {function content_5c6fba1b1f0509_16802000($_smarty_tpl) {?><!DOCTYPE html>
<html class="no-js"  lang="<?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['language'])&&!empty($_smarty_tpl->tpl_vars['meta_data']->value['language'])) {?><?php echo $_smarty_tpl->tpl_vars['meta_data']->value['language'];?>
<?php } else { ?>cs<?php }?>">
    <head>
        <title><?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['title'])&&!empty($_smarty_tpl->tpl_vars['meta_data']->value['title'])) {?><?php echo $_smarty_tpl->tpl_vars['meta_data']->value['title'];?>
<?php } else { ?>BlueGhost<?php }?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['encoding'])&&!empty($_smarty_tpl->tpl_vars['meta_data']->value['encoding'])) {?><?php echo $_smarty_tpl->tpl_vars['meta_data']->value['encoding'];?>
<?php } else { ?>UTF-8<?php }?>" />
        <meta name="description" content="<?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['description'])&&!empty($_smarty_tpl->tpl_vars['meta_data']->value['description'])) {?><?php echo $_smarty_tpl->tpl_vars['meta_data']->value['meta_description'];?>
<?php }?>"/>
        <meta name="keywords" content="<?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['meta_key'])&&!empty($_smarty_tpl->tpl_vars['meta_data']->value['meta_key'])) {?><?php echo $_smarty_tpl->tpl_vars['meta_data']->value['meta_key'];?>
<?php }?>" />

        <?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['index_robots'])&&$_smarty_tpl->tpl_vars['meta_data']->value['index_robots']==true) {?>
            <meta name="robots" content="index, follow"/>

        <?php } else { ?>
            <meta name="robots" content="noindex, nofollow"/>
        <?php }?>

        <meta name="Author" content="Petr Zeleny" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?php if (isset($_smarty_tpl->tpl_vars['meta_data']->value['favicon'])) {?>
            <link rel="Shortcut Icon" href="<?php echo $_smarty_tpl->tpl_vars['meta_data']->value['favicon'];?>
" type="image/x-icon" />
        <?php }?>
        <?php  $_smarty_tpl->tpl_vars['uri'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['uri']->_loop = false;
 $_smarty_tpl->tpl_vars['klic'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['meta_data']->value['css']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['uri']->key => $_smarty_tpl->tpl_vars['uri']->value) {
$_smarty_tpl->tpl_vars['uri']->_loop = true;
 $_smarty_tpl->tpl_vars['klic']->value = $_smarty_tpl->tpl_vars['uri']->key;
?>
            <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['uri']->value;?>
" media="screen, print" />

        <?php } ?>


        <?php  $_smarty_tpl->tpl_vars['uri'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['uri']->_loop = false;
 $_smarty_tpl->tpl_vars['klic'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['meta_data']->value['js']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['uri']->key => $_smarty_tpl->tpl_vars['uri']->value) {
$_smarty_tpl->tpl_vars['uri']->_loop = true;
 $_smarty_tpl->tpl_vars['klic']->value = $_smarty_tpl->tpl_vars['uri']->key;
?>
            <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['uri']->value;?>
" ></script>
        <?php } ?>

    </head><?php }} ?>
