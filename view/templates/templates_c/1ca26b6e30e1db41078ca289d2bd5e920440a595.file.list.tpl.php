<?php /* Smarty version Smarty-3.1.15, created on 2019-02-22 10:00:11
         compiled from "C:\xampp\htdocs\bg_dir\view\templates\themes\blue_ghost\\frontend\person\list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:267675c6fba1b0988b2-47724099%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ca26b6e30e1db41078ca289d2bd5e920440a595' => 
    array (
      0 => 'C:\\xampp\\htdocs\\bg_dir\\view\\templates\\themes\\blue_ghost\\\\frontend\\person\\list.tpl',
      1 => 1550824872,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '267675c6fba1b0988b2-47724099',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'paginator' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5c6fba1b14c3d9_03891086',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c6fba1b14c3d9_03891086')) {function content_5c6fba1b14c3d9_03891086($_smarty_tpl) {?>
<div class="list">

    <?php if (!empty($_smarty_tpl->tpl_vars['items']->value)) {?>
        <table  style="width: 100%;" border="1" class="table table-striped">
            <thead>
                <tr>
                    <th>Jméno</th>
                    <th>Telefon</th>
                    <th>Email</th>
                    <th>Poznámka</th>
                    <th>Akce</th>
                </tr>
            </thead>
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['phone'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['email'];?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['note'];?>
</td>
                    <td>
                        <a href="<?php echo @constant('WEB_URL');?>
/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">Upravit</a>
                        <a href="<?php echo @constant('WEB_URL');?>
/<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
?action=deleteItem">Smazat</a>
                    </td>
                </tr>
            <?php } ?>

        </table>
        <hr/>
        <?php if (isset($_smarty_tpl->tpl_vars['paginator']->value)) {?>
            <h3>Další záznamy</h3>
            <ul>
                <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['paginator']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
"><?php if ($_smarty_tpl->tpl_vars['item']->value['active']) {?><strong><?php echo $_smarty_tpl->tpl_vars['item']->value['number'];?>
</strong><?php } else { ?><?php echo $_smarty_tpl->tpl_vars['item']->value['number'];?>
<?php }?></a></li>
                            <?php } ?> 
            </ul>
        <?php }?>

    <?php } else { ?>
        <strong>Žádné záznamy</strong>
    <?php }?>

    <hr/>
    <a href="?action=addItem">Přidat položku</a>



</div>
<?php }} ?>
