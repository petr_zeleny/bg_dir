<?php /* Smarty version Smarty-3.1.15, created on 2019-02-22 10:00:12
         compiled from "C:\xampp\htdocs\bg_dir\view\templates\themes\blue_ghost\\frontend\person\form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:63945c6fba1c9ff247-88647503%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65f9017c792c353caf2c998079993cf4c7420dfa' => 
    array (
      0 => 'C:\\xampp\\htdocs\\bg_dir\\view\\templates\\themes\\blue_ghost\\\\frontend\\person\\form.tpl',
      1 => 1550786054,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '63945c6fba1c9ff247-88647503',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
    'state' => 0,
    'errors' => 0,
    'error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5c6fba1caddcf9_41849700',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5c6fba1caddcf9_41849700')) {function content_5c6fba1caddcf9_41849700($_smarty_tpl) {?>
<div class="form">
    <?php if (isset($_smarty_tpl->tpl_vars['data']->value['id'])) {?>
        <h2>Editace položky</h2>
    <?php } else { ?>
        <h2>Nová položka</h2>
    <?php }?>

    <form method="post" action="">
        <?php if (isset($_smarty_tpl->tpl_vars['state']->value)&&$_smarty_tpl->tpl_vars['state']->value==true) {?>
            <strong style="color:green;">Záznam byl uložen</strong><br/>

        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['state']->value)&&$_smarty_tpl->tpl_vars['state']->value==false) {?>
            <strong style="color:red;">Záznam nebyl uložen</strong><br/>

        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['error']->key;
?>
                <strong style="color:red"> <?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</strong><br/>
            <?php } ?>
        <?php }?>

        <label for="name">Jméno</label><br/>
        <input type="text" id="name" name="person[name]" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value['name'])) {?><?php echo $_smarty_tpl->tpl_vars['data']->value['name'];?>
<?php }?>">
        <br/>
        <label for="name">Email</label><br/>
        <input type="text" id="email"  name="person[email]" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value['email'])) {?><?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
<?php }?>">
        <br/>
        <label for="name">Telefon</label><br/>
        <input type="text" id="phone"  name="person[phone]" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value['phone'])) {?><?php echo $_smarty_tpl->tpl_vars['data']->value['phone'];?>
<?php }?>">
        <br/>
        <label for="name">Poznámka</label><br/>
        <textarea id="note"  name="person[note]"><?php if (isset($_smarty_tpl->tpl_vars['data']->value['note'])) {?><?php echo $_smarty_tpl->tpl_vars['data']->value['note'];?>
<?php }?></textarea>
        <br/>
        <?php if (isset($_smarty_tpl->tpl_vars['data']->value['id'])) {?>
            <input type="hidden" name="person[id]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
">
        <?php }?>
        <input type="submit" name="save_person" value="<?php if (isset($_smarty_tpl->tpl_vars['data']->value['id'])) {?>Upravit<?php } else { ?>Uložit<?php }?>">
    </form>

    <hr/>
    <a href="<?php echo @constant('WEB_URL');?>
">Zpět na seznam</a>

</div>
<?php }} ?>
